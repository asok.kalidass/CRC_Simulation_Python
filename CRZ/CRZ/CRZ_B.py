
#reference = '11001'
reference = str(raw_input("Please enter reference number G(x):"))
ReferencePolynomial = reference
#MsgReceived = '101100110100'
MsgReceived = str(raw_input("Please enter the recieved message M'(x): "))
Quotient = []
length = len(reference)

#ZFunction to decide the remainder
def decide_remainder ( divisor, divident ):
    global ReferencePolynomial
    if(divident[0] == '1'):
        tempRemainder = 1 
        ReferencePolynomial = reference
    else:
        tempRemainder = 0
        ReferencePolynomial = str([0] * len(reference)).replace(',', '').replace('[', '').replace(']', '')  
        ReferencePolynomial = ReferencePolynomial[:-1].replace(" ", "")
    return tempRemainder

#Divison Validations
if (len(MsgReceived) < len(ReferencePolynomial)):
   print("Long division is not possible")
elif (len(ReferencePolynomial) > len(MsgReceived)):
    print("Long division is not possible")

Quotient.append(decide_remainder(ReferencePolynomial, MsgReceived[:length]))

remainder = str(bin(int(ReferencePolynomial,2) ^ int(MsgReceived[:length], 2)))
remainder = remainder[2:]
count = length
loopCounter = 0
#Logic to perform the long division 
while (loopCounter < (len(MsgReceived) - len(reference))):
    remainder = remainder.zfill(length - 1)
    remainder = remainder + MsgReceived[count]
    Quotient.append(decide_remainder(ReferencePolynomial, remainder))
    remainder = bin(int(ReferencePolynomial, 2) ^ int(remainder,2))
    remainder = remainder[2:]
    count += 1
    loopCounter += 1

print("The Remainder is: ")
print(str(remainder.zfill(length - 1)))
if remainder == '0' :
    print("No Error in Message")
else:
    print("There is Error in Message so, discard the frame")



