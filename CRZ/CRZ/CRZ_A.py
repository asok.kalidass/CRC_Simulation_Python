
#reference = '11001'
reference = str(raw_input("Please enter reference number G(x): "))
ReferencePolynomial = reference
#MsgToBeSent = '10110011'
original_msg = str(raw_input("Please enter the message to be sent M(x): "))
MsgToBeSent = original_msg
remainder = []
length = len(reference)

#ZFunction to decide the remainder
def decide_remainder ( divisor, divident ):
    global ReferencePolynomial
    if(divident[0] == '1'):
        tempRemainder = 1 
        ReferencePolynomial = reference
    else:
        tempRemainder = 0
        ReferencePolynomial = str([0] * len(reference)).replace(',', '').replace('[', '').replace(']', '')  
        ReferencePolynomial = ReferencePolynomial[:-1].replace(" ", "")
    return tempRemainder


#Logic to append the trailing zeros
if ReferencePolynomial[0] == '1':
    MsgToBeSent += str([0] * len(ReferencePolynomial)).replace(',', '').replace('[', '').replace(']', '')
    MsgToBeSent = MsgToBeSent[:-1].replace(" ", "")

#Divison Validations
if (len(MsgToBeSent) < len(ReferencePolynomial)):
   print("Long division is not possible")
elif (len(ReferencePolynomial) > len(MsgToBeSent)):
    print("Long division is not possible")

remainder.append(decide_remainder(ReferencePolynomial, MsgToBeSent[:length]))

quotient = str(bin(int(ReferencePolynomial,2) ^ int(MsgToBeSent[:length], 2)))
quotient = quotient[2:]
count = length
loopCounter = 0
#Logic to perform the long division 
while (loopCounter < (len(MsgToBeSent) - len(reference))):
    quotient = quotient.zfill(length - 1)
    quotient = quotient + MsgToBeSent[count]
    remainder.append(decide_remainder(ReferencePolynomial, quotient))
    quotient = bin(int(ReferencePolynomial, 2) ^ int(quotient,2))
    quotient = quotient[2:]
    count += 1
    loopCounter += 1

#print("The Quotient is: ")
#print(str(remainder).replace(",", ""))
print("The bit string to be transmitted is :" + original_msg + quotient.zfill(length - 1))


